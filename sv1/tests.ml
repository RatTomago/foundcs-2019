(* -------------------------------------------------
 * Use `dune runtest` to run these test cases.
 * Add input/output cases below:
 * ------------------------------------------------- *)

let test_factors =
  (* Pairs of (<input>, <expected_output>) *)
  [
    ([], []);
    ([ 1 ], [ [ 1 ] ]);
    ([ 1; 4; 12 ], [ [ 1 ]; [ 1; 2; 4 ]; [ 1; 2; 3; 4; 6; 12 ] ]);
  ]

let test_set_intersect = [ (([ 1 ], [ 2 ]), []) ]

let test_set_subtract = [ (([ 1; 2 ], [ 1 ]), [ 2 ]) ]

let test_set_union = [ (([ 1 ], [ 2 ]), [ 1; 2 ]) ]

(* -------------------------------------------------
 *  No need to touch anything from here onwards...
 * ------------------------------------------------- *)

let () =
  let uncurry2 f (a, b) = f a b in
  let iotest typ description io_cases fn =
    let check_case index (input, output) =
      let description = "input/output case #" ^ string_of_int index in
      Alcotest.check typ description output (fn input)
    in
    Alcotest.test_case description `Quick (fun () ->
        List.iteri check_case io_cases)
  in
  Alcotest.run "supervision 1"
    [
      ( "factors",
        [
          iotest
            Alcotest.(list (list int))
            "factors" test_factors Questions.Factors.factors;
        ] );
      ( "sets",
        [
          iotest
            Alcotest.(list int)
            "intersect" test_set_intersect
            (uncurry2 Questions.Sets.intersect);
          iotest
            Alcotest.(list int)
            "subtract" test_set_subtract
            (uncurry2 Questions.Sets.subtract);
          iotest
            Alcotest.(list int)
            "union" test_set_union
            (uncurry2 Questions.Sets.union);
        ] );
    ]
