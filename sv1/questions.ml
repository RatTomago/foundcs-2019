(** {1 Foundations of Computer Science}

    {2 Supervision 1 of 4}

    Fill in the various 'TODO' items and submit your work to Craig Ferguson
    [craig@tarides.com] at least 48 hours before the supervision.

    Do _not_ use any helper modules in the standard library ([List], [Set],
    [Hashtbl] etc.). In particular, you should define [filter], [map] and
    [fold] etc. yourself. *)

module Type_inference = struct
  (** 1. Give the inferred types of each of the following functions: *)

  (** type: TODO *)
  let f a _b = a

  (** type: TODO *)
  let g _a b = b

  (** type: TODO *)
  let h a b c =
    if a then
      b
    else
      c

  (** type: TODO *)
  let i a b = a b

  (** type: TODO *)
  let j a b c = a c (b c)
end

module Type_driven_development : sig
  (** 2. For each of the following function types, either give a function of
      that type or argue informally why this cannot be done.

      N.B. you may not use any functions from the standard library ([failwith],
      [invalid_arg], etc.), exceptions or type annotations. *)

  val f : 'a -> unit

  val g : ('a -> 'b) -> 'a -> 'b

  val h : 'a list * 'b list -> ('a * 'b) list

  val i : 'a list -> 'b list
end = struct
  let f _ = failwith "TODO?"

  let g _ = failwith "TODO?"

  let h _ = failwith "TODO?"

  let i _ = failwith "TODO?"
end

module Mystery_complexities = struct
  (** 3. For each of the following functions:

      - give a concise description of their behaviour
      - state their space and time complexities in terms of their
        non-functional parameters.

      Assume that the compiler performs no optimisations on the given code. *)

  (** description: TODO

      {[
        +--------------+-------------------------+------------------------+
        | parameter, p | space complexity, s(p)  | time complexity, t(p)  |
        +--------------+-------------------------+------------------------+
        | l            | TODO                    | TODO                   |
        +--------------+-------------------------+------------------------+
      ]} *)
  let rec f pred l =
    match l with
    | [] -> 0
    | x :: xs ->
        if pred x then
          1
        else
          0 + f pred xs

  (** description: TODO

      {[
        +--------------+-------------------------+------------------------+
        | parameter, p | space complexity, s(p)  | time complexity, t(p)  |
        +--------------+-------------------------+------------------------+
        | l            | TODO                    | TODO                   |
        +--------------+-------------------------+------------------------+
      ]} *)
  let f' pred l =
    let rec inner l acc =
      match l with
      | [] -> acc
      | x :: xs ->
          let inc =
            if pred x then
              1
            else
              0
          in
          inner xs (acc + inc)
    in
    inner l 0

  (** description: TODO

      {[
        +--------------+-------------------------+------------------------+
        | parameter, p | space complexity, s(p)  | time complexity, t(p)  |
        +--------------+-------------------------+------------------------+
        | n            | TODO                    | TODO                   |
        +--------------+-------------------------+------------------------+
      ]} *)
  let rec g n =
    if n > 1 then
      g (n - 1) + g (n - 2)
    else
      n

  (** description: TODO

      {[
        +--------------+-------------------------+------------------------+
        | parameter, p | space complexity, s(p)  | time complexity, t(p)  |
        +--------------+-------------------------+------------------------+
        | l            | TODO                    | TODO                   |
        +--------------+-------------------------+------------------------+
      ]} *)
  let rec h l =
    let rec id_map xs =
      match xs with
      | [] -> []
      | x :: xs -> x :: id_map xs
    in
    match l with
    | [] -> []
    | _ :: tl -> id_map l :: h tl
end

module Factors : sig
  (** 4. Write a function [factors] that converts a list of integers into a
      list of lists of the factors of those integers. For example,

      {[ factors [1; 4; 12] = [[1]; [1; 2; 4]; [1; 2; 3; 4; 6; 12]] ]}

      The order of the factors within each internal list is not important. *)
  val factors : int list -> int list list
end = struct
  let factors _ = failwith "TODO"
end

module Sets : sig
  (** 1995 Paper 1 Question 3 *)

  (** 5. An OCaml list can be considered to be a set if it has no repeated
      elements, e.g., [4; 2; 3] is a set but [4; 2; 4; 3] is not.

      Write an ML function [intersect] to compute the set-theoretic
      intersection of two lists that satisfy this property of being a set. (The
      intersection of two sets is the set of elements that appear in both
      sets.) Your function must also satisfy conditions (a)--(c) below:

      (a) The result list has no repeated elements;

      (b) The number of cons (::) operations performed does not exceed the
      number of elements in the result list.

      (c) The elements of the result list appear in the same order as they do
      in the first argument list.

      You may assume the existence of [] (nil) and :: (cons). All other
      functions over lists must be defined by you. Little credit will be given
      for answers that do not satisfy conditions (a)--(c). *)
  val intersect : int list -> int list -> int list

  (** Write an OCaml function [subtract] that given two lists satisfying the
      property of being a set, returns a list consisting of those elements of
      the first list that do not appear in the second list. Your subtract
      function must satisfy conditions (a)--(c) above. *)
  val subtract : int list -> int list -> int list

  (** Write an OCaml function [union] that given two lists satisfying the
      property of being a set, returns a list consisting of those elements that
      appear in one or other or both of the lists. Your union function must
      satisfy conditions (a)--(c) above and (d) below:

      (d) Elements from the first argument list appear before any others in the
      result. *)
  val union : int list -> int list -> int list
end = struct
  let intersect _ = failwith "TODO"

  let subtract _ = failwith "TODO"

  let union _ = failwith "TODO"
end
