# Foundations of Computer Science

This repostory will eventually contain the supervision work for all four of my
FoundCS supervisions (in `sv{1,2,3,4}/questions.ml`). Either clone it using
`git` or [download the source code as a `.zip` file][zip-url] and unzip it.

The `questions.ml` files contain some OCaml syntax that you are not expected to
learn (and will not show up in the exam). The `sig ... end = struct ... end`
blocks give the names and types of the top-level functions that you are expected
to implement, as in the following example:

```ocaml
module Question_about_lists : sig
  (* Here are the types of the functions that you should implement *)
  
  val map : ('a -> 'b) -> 'a list -> 'b list
end = struct
  (* Implement the functions within [sig ... end] here. They must have the same
     names and types as defined above or the OCaml compiler will complain. 
     
     You can also define helper functions / values in here, as long as you 
     define _at least_ the values specified above. *)
   
   let map _ = failwith "TODO" (* initially blank *)
end
```

This pre-defined structure makes it easier for me to mark your work.

## Booking supervisions

You will have four supervisions for this course. You should book each
supervision in the corresponding time-slot:
 
  1. 19--20 October
  2. 26--27 October
  3. 9--10 November
  4. 16--17 November
  
If you are unable to book supervisions for these times, let me know as soon as
possible and I'll try to accommodate you.

## Submitting work

Work should be sent to `craig(at)tarides(dot)com`. 

Any code exercises should be sent in `.ml` files (potentially in addition to
whatever LaTeXed `.pdf` / `.md` / `txt` document you produce). Please make sure
that your code compiles before sending it to me!

## Testing your solutions

You are encouraged to write tests for any solutions that you write. Each
`questions.ml` file comes with a corresponding `tests.ml` file containing
example tests to get you started. Once you have set up a development
environment, you can run these tests with `dune runtest`.

## Setting up an OCaml development environment

The notes for this course use a Jupyter notebooks workflow to teach you OCaml. I
recommend setting up a local development environment of your own for editing
OCaml, but this is not compulsory. This involves [installing
opam][opam-install], the OCaml package manager. The easiest way to do so is to
run:

```shell
sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)
```

Once you have done so, install the OCaml build system (`dune`) and the test
dependencies (`alcotest`) with:

```shell
opam install dune alcotest
```

You can now use dune to compile and test your supervision work solutions:

```shell
dune build --force       # Compile all `*.ml` files
dune runtest             # Compile and run all tests
dune exec sv1/tests.exe  # Compile and run only tests for SV1
```

### Windows

If you are running Windows, I highly recommend setting up a UNIX environment to
get access to useful utilities like `sh`, `cat` and `git`. You'll find it very
useful throughout the Tripos. You have several options for doing so (listed in
rough order of personal preference):

 - __Windows Subsystem for Linux__. If on Windows 10, enable the [Windows
   Subsystem for Linux][wsl-install] to have access to useful Unix utilities
   like `sh` and `git`. You can then install `opam` using the `sh` command given
   above. You will need to install a C compiler (using `apt install gcc`). You
   may need to pass `--disable-sandboxing` to `opam init`.
   
 - __Remote access to a Managed Cluster Service machine__. The Computer Lab
   gives students access to Linux machines via [SSH][ssh] (see full instructions
   [here][mcs-remote]). Install an SSH client such as [PuTTY][putty] and SSH
   into `<crsid>@linux.ds.cam.ac.uk`.
 
 - __Install a Linux virtual machine__. Install [VirtualBox][vbox] and a Linux
   OS image (such as [Ubuntu 18.04 LTS][ubuntu-install]).
   
 - __Local access to a Managed Cluster Service (MCS) machine__. You can use the
   MCS machines in-person at your college's Computer Room or in the Intel Lab at
   the CL.
   
[ssh]: https://www.ssh.com/ssh/
[putty]: https://www.ssh.com/ssh/putty/
[vbox]: https://www.virtualbox.org/
[ubuntu-install]: https://ubuntu.com/download/desktop
[mcs-remote]: https://help.uis.cam.ac.uk/service/desktop-services/mcs/remotelinux
[wsl-install]: https://docs.microsoft.com/en-us/windows/wsl/install-win10
[opam-install]: https://opam.ocaml.org/doc/Install.html
[zip-url]: https://gitlab.com/CraigFe/foundcs-2019/-/archive/master/foundcs-2019-master.zip
